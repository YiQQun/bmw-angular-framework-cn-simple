import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BMW, BMWModalService } from 'bmw-angular-framework-cn-ext';

@NgModule({
    imports: [
        BMW,
        CommonModule,
        FormsModule,
    ],
    declarations: [
    ],
    providers: [
        BMWModalService,
    ],
    exports: [
        CommonModule,
        FormsModule,
        BMW,

    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    entryComponents: [
        
    ]
})

export class SharedModule { }
