import { Component, OnInit } from '@angular/core';
import { NativeStylingService } from 'bmw-angular-framework-cn-ext';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
    constructor(private frameworkStylingService: NativeStylingService) { }
    ngOnInit(): void {
        this.frameworkStylingService.init();
    }
}
