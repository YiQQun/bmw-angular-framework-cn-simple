// Helper: root() is defined at the bottom
var path = require('path');
var webpack = require('webpack');

// Webpack Plugins
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ZipPlugin = require('zip-webpack-plugin')
var AngularCompilerPlugin = require('@ngtools/webpack').AngularCompilerPlugin;
var ProgressPlugin = require('webpack/lib/ProgressPlugin');
var UglifyJsWebpackPlugin = require('uglifyjs-webpack-plugin');
/**
 * Env
 * Get npm lifecycle event to identify the environment
 */
var ENV = process.env.npm_lifecycle_event;
var isProd = ENV === 'build:aot' || ENV === 'build:jit';
var isAot = ENV === 'build:aot';
var isJit = ENV === 'build:jit';
var noEmitOnErrors = false;

module.exports = function makeWebpackConfig() {
    //configs
    var config = {};
    config.node = {
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    }
    var htmlTemplate = '';
    if(isProd) {
        config.mode = 'production'
        noEmitOnErrors = true;
        htmlTemplate = './index-jit.html'
    } else {
        config.mode = 'development'
        config.cache = true
        noEmitOnErrors = false;
        htmlTemplate = './index.html'
    }
    //entry
    config.entry =  {
        'polyfills': './src/polyfills.ts',
        'vendor': './src/vendor.ts',
        'app': './src/main.ts'
    };
    
    //outputs
    config.output = {
        path: root('dist'),
        filename: isProd ? 'js/[name].[hash].js' : 'js/[name].js',
        chunkFilename: isProd ? 'js/[id].[hash].chunk.js' : 'js/[id].chunk.js'
    };
    
    //exclude resolve 
    config.resolve = {
        extensions: ['.ts', '.js', '.json', '.css', '.scss', '.html'],
    };

    //defult resource loader
    config.module = {
        rules: [
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader?name=public/assets/images/[name].[ext]'
            },
            { 
                test: /[\/\\]@angular[\/\\].+\.js$/, parser: { system: true }
             },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: ['css-loader'] })
            },
            { 
                test: /\.css$/, include: root('src', 'app'), 
                loader: 'raw-loader' 
            },
            {
                test: /\.(scss|sass)$/,
                include: root('src/app/styles'),
                loader:  ExtractTextPlugin.extract({ fallback: ['style-loader'], use: ['css-loader', 'sass-loader'] })
            },
            {
                test: /\.(scss|sass)$/,
                exclude: root('src/app/styles'),
                loaders: ['raw-loader', 'sass-loader']
            },
            { 
                test: /\.html$/, loader: 'raw-loader', 
            }
        ]
    };
    
    //ts loaders config 
    if(isProd && isAot) {
        //aot loader
            config.module.rules.push({
            test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
            loaders: ['@ngtools/webpack', 'angular2-template-loader']
        })
    } else {
        //jit loader
        console.log(isProd);
        console.log(isAot);
        console.log('build jit')
        config.module.rules.push(
            {
                test: /\.ts$/,
                loaders: ['ts-loader', 'angular2-template-loader', '@angularclass/hmr-loader', 'angular-router-loader'],
                exclude: /node_modules/,
            }
        )
    }

    //defult plugins
    config.plugins = [
        new ProgressPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ContextReplacementPlugin(
            /\@angular(\\|\/)core(\\|\/)fesm5/,
            root('./src')
        ),
        new ExtractTextPlugin({ filename: '[name].css'}),
        new HtmlWebpackPlugin({
            template: htmlTemplate,
            chunksSortMode: 'dependency',
            excludeChunks:['bmw.ios.style', 'bmw.android.style', 'mini.ios.style'],
        })
    ]
    //optimizaiton: splitChunks && minizer ..
    if(isProd) {
        console.log("copy plugin");
        config.optimization =  { //代码分包，暂时没用到
            noEmitOnErrors: noEmitOnErrors,
            splitChunks: {
                chunks: 'all' //unused
                // minSize: 30000,
                // minChunks: 1,
                // maxAsyncRequests: 5,
                // maxInitialRequests: 3,
                // automaticNameDelimiter: '~',
                // name: true,
                // cacheGroups: {
                //     vendor: {
                //         name: 'vendor',
                //         test: /[\\/]node_modules[\\/]/,
                //         priority: -10,
                //         enforce: true
                //     },
                //     polyfills: {
                //         name: 'polyfills',
                //         test: /[\\/]node_modules[\\/]/,
                //         priority: -10,
                //         enforce: true
                //     }
                // }
            },
            minimizer: [
                new UglifyJsWebpackPlugin({
                  parallel: true,
                  sourceMap: false,
                  uglifyOptions: {
                    warnings: false,
                    parse: {},
                    compress: {},
                    mangle: true, // Note `mangle.properties` is `false` by default.
                    output: null,
                    toplevel: false,
                    nameCache: null,
                    ie8: false,
                    keep_fnames: false,
                    output: {
                      comments: false
                    },
                    mangle: true,
                  },
                  extractComments: true
                })
            ]
        }
    } else {
        config.optimization = {
            noEmitOnErrors: noEmitOnErrors,
            minimizer: []
        }
    }   

    //plugins for prod mode
    if(isProd) {
        if(isAot) {
            config.plugins.push(
                new AngularCompilerPlugin({
                    tsConfigPath: './tsconfig.aot.json',
                    entryModule: './src/app/app.module#AppModule',
                })
            )
        }
       
        config.plugins.push(
            new CopyWebpackPlugin([
                {
                    from: root('public/**/*'),
                },
                {
                    from: root('public/**/**/*'),
                },
                {
                    from: root('./node_modules/applicationinsights-js/dist/**.js'),
                },
                {
                    from: root('./node_modules/crypto-js/**.js'),
                },
                {
                    from: root('./node_modules/bmw-angular-framework-cn-ext/src/animations/**.css'),
                },
                {
                    from: root('./node_modules/bmw-angular-framework-cn-ext/public/**/*'),
                },
                {
                    from: root('./node_modules/bmw-angular-framework-cn-ext/src/**.css')
                }
            ]),
            new ZipPlugin({
                path: root('dist'),
                filename:'build.zip'
            })
        );
    } 

    //devServer
    config.devServer = {
        contentBase: './',
        historyApiFallback: true,
        quiet: false,
        stats: 'minimal'
    };

    return config;
}();

function root(args) {
    args = Array.prototype.slice.call(arguments, 0);
    return path.join.apply(path, [__dirname].concat(args));
}