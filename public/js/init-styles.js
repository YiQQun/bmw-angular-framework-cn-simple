var path = 'public/framework7/1.6.5/css/';

/* OS Constants */
var OS = {
    ios: 'ios',
    android: 'android'
};

var styles = {
    ios: [
        path + 'framework7.ios.min.css',
        path + 'framework7.ios.colors.min.css',
        'public/styles/styles.css'
    ],
    android: [
        path + 'framework7.material.min.css',
        path + 'framework7.material.colors.min.css',
        'public/styles/styles.css'
    ]
};

var mobileOS = getMobileOperatingSystem();

styles[mobileOS].map(function (styleURL) {
    var el = document.createElement('link');
    el.rel = 'stylesheet';
    el.href = styleURL;
    document.head.appendChild(el);
});

document.addEventListener('gesturestart', function (e) {
    e.preventDefault();
});

function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    var queryParams = window.location.search;
    if (queryParams.includes('target' && OS.android) || /android/i.test(userAgent)) {
        return OS.android;
    }
    return OS.ios; 
}