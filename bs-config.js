module.exports = {
  "port": 8080,
  "ghostMode": false,
  "open": false,  
  "notify": false,
  "server": {
    "baseDir": [      
      "dist"
    ]
  }
};
